$(document).ready(function(){

    var list = ['seoul','gyeonggi','incheon','daejeon','daegu','gwangju','busan',
    'ulsan','sejong','gangwon','chungbuk','chungnam','jeollabuk','jeollanam','gyeongbuk','gyeongnam','jeju'];

    var btn = document.getElementById("myBtn");
    // Get the modal
    var modal = document.getElementsByClassName("modal");
    var modal1 = document.getElementById('child1_modal');
    var modal2 = document.getElementById('child2_modal');
    var modal3 = document.getElementById('child3_modal');
    var modal4 = document.getElementById('child4_modal');

    $('.dropbtn').on('click',function(){
        $('#myModal').css('display','block');
    });

    jQuery(document).ready(function(){
        //시,도 클릭했을 때
        $('.big_region').on('click', function(e){
          
            selected_big = $(this).attr("id");
            
            $('.big_region').css('backgroundColor','white');
            $(this).css('backgroundColor','#efede4');
            $('.small_region').css('backgroundColor','white');
            

            for(var i=0; i<list.length; i++){
                if (list[i] == selected_big){
                    $('#'+selected_big+'region').css('display','inline-block');

                }
                else{
                    $('#'+list[i]+'region').css('display','none');
                }
            }
        });

        //시,군,구 클릭했을 때
        $('.small_region').on('click', function(e){
            $('.small_region').css('backgroundColor','white');
            $(this).css('backgroundColor','#efede4');
            $('.confirm_1').css('backgroundColor','#382827');
        });

        var selected_region = ''
        $('.confirm_1').on('click', function(e){
            if ( $('.confirm_1').isBgColor('#382827')){
                $('.big_region').each(function(){
                    if($(this).isBgColor("#efede4")){
                        var selected_big = $(this).text();
                        selected_region = selected_big;
                    };
                });
                $('.small_region').each(function(){
                    if($(this).isBgColor("#efede4")){
                        var selected_small = $(this).text();
                        selected_region = selected_region +" "+ selected_small;
                    };
                });
    
                $('.dropbtn').val(selected_region);
                $('.dropbtn').css('color','#382827');
                $('#myModal').css('display','none');
                $('.warning').css('display','none');
                $('.next').css('backgroundColor','#382827');
            }
        });

    });

    // 다음으로 눌렀을때, 지역선택 제대로했는지 확인. 선택했다면 다음input창으로
    $('.next').on('click',function(){
        if ($('.next').isBgColor('#382827')){
            // 시도, 시군구 모두 선택안했을 경우
            if($('.dropbtn').val().length < 5){
                $('.warning').eq(1).css('display','block');
            }
            else{
                $('#Input2').css('display','block');
                $('#Input1').css('display','none');
            };
            
        }
        else{
            $('.warning').eq(0).css('display','block');
        }
    });
    //Inpu2 js

    //출생년월 선택
    $('.birth').on('click',function(e){
        var selected_child = $(this).parent().attr("id");
        var selected_modal = selected_child + "_modal";
        $('#'+selected_modal).css('display','block');
    });

    $('.year').on('click',function(e){
        $(this).siblings('.year').css('backgroundColor','white');
        $(this).css('backgroundColor','#efede4');
        var s = $(this).parent().parent().parent().find('.month-grid-item');

        s.each(function(){
            if ($(this).isBgColor("#efede4")){
                var my_confirm = $(this).parent().parent().next('.confirm_2');
            }
        });
    });

    $('.month-grid-item').on('click',function(e){
        $(this).siblings('.month-grid-item').css('backgroundColor','white');
        $(this).css('backgroundColor','#efede4');
        var s = $(this).parent().parent().find('.year');
        
        s.each(function(){
            if ($(this).isBgColor("#efede4")){
                var my_confirms = $(this).parent().parent().parent().next('.confirm_2');
                my_confirms.css('backgroundColor','#382827');
            }
        });
    });

    

    $('.confirm_2').on('click', function(e){
        //연도, 월 둘 다 눌렸을 경우.
        if ($(this).isBgColor('#382827')){
            var selected_birth = '';
            var bro = $(this).siblings('div').eq(0).find('.year');
            var bro2 = $(this).siblings('div').eq(0).find('.month-grid-item'); 
            
            bro.each(function(){
                if ($(this).isBgColor("#efede4")){
                    var selected_year = $(this).text();
                    selected_birth = selected_year;
                }
            });
            bro2.each(function(){
                if($(this).isBgColor("#efede4")){
                    var selected_month = $(this).text();
                    selected_birth = selected_birth + "." + selected_month;
                }
            });
        
            
            var selected_id = $(this).parent().parent().attr("id").split("_")[0];
        
            $("#" + selected_id + "> input").val(selected_birth);
            $('#' + selected_id + "> input").css('font-size','1.125rem');
            $('#' + selected_id + "> span").css('color','#382827');
            $('#' + selected_id + "> input").css('color','#382827');
            //모달 닫기
            $(this).parent().parent().css('display',"none");
        }
        else{
           // nothing happen
        }
        
        //확인버튼 활성화
        $(".birth").each(function(){
            //일단 갈색으로 완료 버튼 활성화시킨 후
            $('.complete').css('backgroundColor','#382827');
            if($(this).parent().css('display') != 'none' && $(this).val().length < 6){
                //자식인데 탄생년월 제대로 체크안했을 시, 다시 회색으로.
                $('.complete').css('backgroundColor','#dedbd3');
                return false;
            }
        });
    
    });

    // When the user clicks anywhere outside of the modal, close it
    document.onclick = function(event) {
        if (event.target == modal1) {
            modal1.style.display = "none";
        } 
        else if(event.target == modal2) {
            modal2.style.display = "none";      
        }
        else if(event.target == modal3) {
            modal3.style.display = "none";      
        }
        else if(event.target == modal4) {
            modal4.style.display = "none";      
        }
    }

    
    

    // select 커스터마이징 customizing
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      /*for each element, create a new DIV that will act as the selected item:*/
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
        //j=0부터하면 자녀수선택도 선택에 있음
      for (j = 1; j < selElmnt.length; j++) {  
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.style.color = '#382827';
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            
            $('.select-selected').eq(0).toggleClass("active");
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                h.style.color = '#382827';
                y = this.parentNode.getElementsByClassName("same-as-selected");
                for (k = 0; k < y.length; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");

                // 선택에 따른 출생년월 div 생성,삭제
                var children_num = parseInt(this.innerHTML);
                for(var j=2; j<6; j++){
                    var notchild = "child" + j;
                    $('#'+notchild).css('display','none');
                };
                for(var i=1; i<= children_num;  i++){
                    var child = "child" + i;
                    $('#'+child).css('display','table');
                };

                //확인버튼 활성화
                $(".children_birth").each(function(){
                    //일단 갈색으로 완료 버튼 활성화시킨 후
                    $('.complete').css('backgroundColor','#382827');
                    if($(this).css('display') != 'none' && $(this).children("div").text().length < 6 ){
                        
                        //자식인데 탄생년월 제대로 체크안했을 시, 다시 회색으로.
                        $('.complete').css('backgroundColor','#dedbd3');
                        return false;
                    }
                });
                // 1 선택시
                if( children_num == 1 ){
                    $('#child1_disable').css('display','none');
                    $('#child2_disable').css('display','table');
                }
                else if(children_num == 2){
                    $('#child1_disable').css('display','none');
                    $('#child2_disable').css('display','none');
                }
                else{
                    $('#child1_disable').css('display','none');
                    $('#child2_disable').css('display','none');
                }
                if (children_num >2){
                    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
                    return false;
                }

                

                break;
              }
            }
            h.click();


        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
          
          /*when the select box is clicked, close any other select boxes,
          and open/close the current select box:*/
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("select-hide");
          this.classList.toggle("select-arrow-active");
      });
    }
    function closeAllSelect(elmnt) {

        
      /*a function that will close all select boxes in the document,
      except the current select box:*/
      var x, y, i, arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");

      $('.select-selected').eq(0).toggleClass("active");


      for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i)
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
    
    // select 끝
});
    // When the user clicks anywhere outside of the modal, close it
    document.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };



    // backgroundcolor check function
    (function($) {
        $.fn.isBgColor = function(color) {
            var thisBgColor = this.eq(0).css('backgroundColor');
            var computedColor = $('<div/>').css({ 
                backgroundColor: color
            }).css('backgroundColor');
            return thisBgColor === computedColor;
        }
    })(jQuery);

    